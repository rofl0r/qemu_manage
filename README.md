Qemu Manage [qemu ncurses interface]
===========

## Features
 * Add guest
 * Delete guest
 * Clone guest
 * Show guest status
 * Start/stop guest
 * Connect to guest via vnc
 * Show/Edit guest hardware parametrs
 * USB support

## Videos
[![Alt Add VM example](http://img.youtube.com/vi/jOtCY--LEN8/1.jpg)](http://www.youtube.com/watch?v=jOtCY--LEN8)

## Environment Requirements
 * Linux host
 * app-emulation/qemu
 * net-misc/openvswitch [optional]

[Download latest release (0.1.7)](https://bitbucket.org/PascalRD/qemu-manage/downloads/qemu-manage-0.1.7-amd64.deb)