#ifndef QM_WINDOWS_H_
#define QM_WINDOWS_H_

#include "base_form_window.h"
#include "install_window.h"
#include "edit_window.h"
#include "add_vm_window.h"
#include "clone_window.h"
#include "add_disk_window.h"
#include "edit_net_window.h"

#endif
